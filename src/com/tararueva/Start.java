package com.tararueva;

import com.tararueva.demoService.DemoServiceImpl;

public class Start {

    public static void main(String[] args) {

        DemoServiceImpl demoServiceImpl = new DemoServiceImpl();
        demoServiceImpl.showDemo();

    }
}
