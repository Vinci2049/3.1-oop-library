package com.tararueva.demoService;

import com.tararueva.data.Library;
import com.tararueva.data.Book;
import com.tararueva.libraryService.LibraryServiceImpl;

public class DemoServiceImpl implements DemoService {

    public void showDemo() {

        // инидиализируем массив книг
        Book[] books = {new Book("Это я, Эдичка", "Эдуард Лимонов"),
                new Book("Сто лет одиночества", "Габриэль Гарсия Маркес"),
                new Book("Превращение", "Франц Кафка"),
                new Book("Омон Ра", "Виктор Пелевин"),
                new Book("Петсон и Финдус", "Свен Нурдквист"),
                new Book("Слова", "Жан-Поль Сартр"),
                new Book("Легенды Невского проспекта", "Михаил Веллер"),
                new Book("Королева Марго", "Александр Дюма"),
                new Book("Буксир", "Иосиф Бродский"),
                new Book("Зеленая корова", "Марсель Эме")};

        // создаем библиотеку
        Library library = new Library("Greate Library of Izhevsk", books);

        // Инициализируем сервис
        LibraryServiceImpl libraryService = new LibraryServiceImpl();

        // выводим получившийся список книг
        System.out.println("Первоначально заполняем библиотеку следующими книгами: ");
        libraryService.printListOfBooks(library);

        // удаляем книги
        String nameForDeleting1 = "Омон Ра";
        String nameForDeleting2 = "Буксир";
        libraryService.deleteBookByName(library, nameForDeleting1);
        libraryService.deleteBookByName(library, nameForDeleting2);

        System.out.println();
        System.out.println("Удаляем книги со следующими названиями: ");
        System.out.println(nameForDeleting1);
        System.out.println(nameForDeleting2);
        System.out.println();

        System.out.println("Список книг: ");
        libraryService.printListOfBooks(library);
        System.out.println();

        // добавляем книги
        Book book1 = new Book("Крошка Цахес по прозванию Циннобер", "Эрнст Теодор Амадей Гоффман");
        Book book2 = new Book("Письмена бога", "Хорхе Луис Борхес");
        Book book3 = new Book("Темная башня", "Стивен Кинг");

        System.out.println("Добавляем следующие книги: ");
        book1.printBook();
        book2.printBook();
        book3.printBook();

        libraryService.addBook(library, book1);
        libraryService.addBook(library, book2);
        libraryService.addBook(library, book2);

        System.out.println();
        System.out.println("Получился список книг: ");
        libraryService.printListOfBooks(library);
        System.out.println();


        // ищем книги
        String searchingName1 = "Королева Марго";
        String searchingName2 = "Пролетая над гнездом кукушки";
        System.out.println("Ищем книгу с названием: "+searchingName1);

        Book searchingBook1 = libraryService.searchBookByName(library, searchingName1);
        if (searchingBook1 != null) {
            System.out.println("Нашли! Автор книги: " + searchingBook1.getAuthor());
        }
        else {
            System.out.println("Не нашли");
        }

        System.out.println();


        System.out.println("Ищем книгу с названием: "+searchingName2);
        Book searchingBook2 = libraryService.searchBookByName(library, searchingName2);
        if (searchingBook2 != null) {
            System.out.println("Нашли! Автор книги: " + searchingBook2.getAuthor());
        }
        else {
            System.out.println("Не нашли");
        }

    }

}
