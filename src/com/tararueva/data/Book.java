package com.tararueva.data;

public class Book {

    private static int maxId = 0;

    private int id;
    private String name;
    private String author;

    public Book(String name, String author) {
        // наверное надо проверить это на not null
        this.id = ++maxId;
        this.name = name;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public void printBook() {
        System.out.println("Название: "+this.getName()+"; Автор: "+this.getAuthor());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (getId() != book.getId()) return false;
        if (!getName().equals(book.getName())) return false;
        return getAuthor().equals(book.getAuthor());
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getAuthor().hashCode();
        return result;
    }
}
