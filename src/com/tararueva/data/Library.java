package com.tararueva.data;

import com.tararueva.data.Book;

public class Library {

    private Book[] books;
    private String name;

    public Library(String name, Book[] books) {
        this.name = name;
        this.books = books;
    }

    public Book[] getBooks() {
        return books;
    }

    public String getName() {
        return name;
    }
}
