package com.tararueva.libraryService;

import com.tararueva.data.Book;
import com.tararueva.data.Library;

interface LibraryService {

   Book addBook(Library library, Book book);

   boolean deleteBookByName(Library library, String name);

   Book searchBookByName(Library library, String name);

   void printListOfBooks(Library library);

}
