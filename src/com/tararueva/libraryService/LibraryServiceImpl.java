package com.tararueva.libraryService;

import com.tararueva.data.Book;
import com.tararueva.data.Library;

public class LibraryServiceImpl implements LibraryService {

    @Override
    public Book addBook(Library library, Book book) {
        Book[] books = library.getBooks();
        for (int i=0; i<books.length; i++) {
            if (books[i] == null) {
                books[i] = book;
                return book;
            }
        }
        return null;
    }

    @Override
    public boolean deleteBookByName(Library library, String name) {
        Book[] books = library.getBooks();
        for (int i=0; i<books.length; i++) {
            if (books[i] != null && books[i].getName().equals(name)) {
                books[i] = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public Book searchBookByName(Library library, String name) {
        Book[] books = library.getBooks();
        for (Book book : books) {
            if (book !=null && book.getName().equals(name)) {
                return book;
            }
        }
        return null;
    }

    @Override
    public void printListOfBooks(Library library) {
        int i = 0;
        Book[] books = library.getBooks();
        System.out.printf("%-4s %-60s%-30s%n", "Ном", "Название", "Автор");
        System.out.println("------------------------------------------------------------------------------------------");
        for (Book book : books) {
            if (book != null) {
                i++;
                System.out.printf("%-4s %-60s%-30s%n", Integer.toString(i) + ".", book.getName(), book.getAuthor());
            }
        }
    }

}
